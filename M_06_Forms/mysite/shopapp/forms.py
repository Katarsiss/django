from django import forms
# from django.core import validators
from .models import Product, Order


# class ProductForm(forms.Form):
#     name = forms.CharField(max_length=100)
#     price = forms.DecimalField(min_value=1, max_value=300000, decimal_places=2)
#     description = forms.CharField(widget=forms.Textarea(attrs={"rows": 5, "cols": 30}),
#                                   label="Product description",
#                                   validators=[validators.RegexValidator(
#                                       regex=r"great", message="Field must contain word 'great'",
#                                   )],)
class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = "name", "price", "description", "discount"


# class OrderForm(forms.Form):
#     username = forms.CharField(max_length=50)
#     delivery_address = forms.CharField(max_length=200)
#     promocode = forms.CharField(max_length=20)
#     products = forms.CharField(widget=forms.CharField, label="Product description")

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = "user", "delivery_address", "promocode", "products"
