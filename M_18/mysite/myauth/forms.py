from django import forms
from django.contrib.auth.models import User, Group, Permission
from django.forms import ModelForm, Textarea

from myauth.models import Profile


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = "bio", "agreement_accepted", "avatar"
        widgets = {
            "bio": Textarea(attrs={"cols": 50, "rows": 20}),
        }
