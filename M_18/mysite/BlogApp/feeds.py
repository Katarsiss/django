from django.contrib.syndication.views import Feed
from django.db.models import QuerySet
from django.urls import reverse
from BlogApp.models import Article


class LatestArticlesFeed(Feed):
    title = 'Статьи'
    link = '/blog/'
    description = "Самые свежие статьи"

    def items(self) -> QuerySet:
        return Article.objects.order_by('-pub_date')[:5]

    def item_title(self, item: Article) -> str:
        return item.title

    def item_description(self, item: Article) -> str:
        return item.content

    def item_link(self, item: Article) -> str:
        return reverse('BlogApp:article', args=[item.pk])
