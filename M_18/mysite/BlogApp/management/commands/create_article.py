from typing import Sequence
from django.core.management import BaseCommand
from django.db import transaction
from BlogApp.models import Article, Author, Tag, Category


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write("Create article with all atributes")
        user = Author.objects.get(name="Monte Christo")
        category = Category.objects.get(name="News")
        tags: Sequence[Tag] = Tag.objects.only("pk", "name").all()
        article, created = Article.objects.get_or_create(
            title="The most urgent news",
            author=user,
            category=category,
            content='Many words and sentences'
        )
        for tag in tags:
            article.tags.add(tag)
        article.save()
        self.stdout.write(f"Created {article}")
