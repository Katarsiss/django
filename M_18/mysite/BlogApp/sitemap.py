from django.contrib.sitemaps import Sitemap
from BlogApp.models import Article


class BlogSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        return Article.objects.all()

    def lastmod(self, obj: Article):
        return obj.pub_date
