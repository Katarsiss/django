from timeit import default_timer
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render


def shop_index(request: HttpRequest):
    first_header = "Hello! This is a Shop Index page!"
    context = {
        "time_running": default_timer(),
        "first_header": first_header
    }
    return render(request, 'shopapp/shop-index.html', context=context)
