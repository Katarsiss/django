from typing import Sequence

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db import transaction
from shopapp.models import Order, Product


# class Command(BaseCommand):
#     @transaction.atomic
#     def handle(self, *args, **options):
#         self.stdout.write("Create order with products")
#         user = User.objects.get(username="katar")
#         # products: Sequence[Product] = Product.objects.defer("description", "price", "created_at", "is_instock").all()
#         products: Sequence[Product] = Product.objects.only("pk", "name").all()
#         order, created = Order.objects.get_or_create(
#             delivery_address="ul Pupkina, d 16",
#             promocode="PROMO5",
#             user=user,
#         )
#         for product in products:
#             order.products.add(product)
#         order.save()
#         self.stdout.write(f"Created {order}")


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write("Create order with products")
        user = User.objects.get(username="katar")
        products: Sequence[Product] = Product.objects.only("pk", "name").all()
        count, i = 0, 1
        while count != 101:
            order, created = Order.objects.get_or_create(
                delivery_address=f"ul Pushkina, d {i}",
                promocode="PROMO5",
                user=user,
                )
            for product in products:
                order.products.add(product)
            order.save()
            count += 1
            i += 1
            self.stdout.write(f"Created {order}")
        self.stdout.write(f"Created {count - 1} orders")
