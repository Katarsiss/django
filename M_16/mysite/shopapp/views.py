"""
В этом модуле лежат различные наборы представлений.

Разные view для интернет-магазина: товары, заказы и т.д.
"""
from timeit import default_timer

from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView

from .forms import ProductForm, OrderForm, GroupForm
from .models import Product, Order, ProductImage
from .serializers import ProductSerializer, OrderSerializer
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema, OpenApiResponse
import logging


logger = logging.getLogger(__name__)


# API
@extend_schema(description="Product views CRUD")
class ProductViewSet(ModelViewSet):
    """
    Набор представлений для действий над Product.

    Полный CRUD для действий над товаром.
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter,
    ]
    search_fields = ["name", "description"]
    filterset_fields = [
        "name",
        "description",
        "price",
        "discount",
        "archived",
    ]
    ordering_filters = [
        "name",
        "price",
        "discount",
        "archived",
    ]

    @extend_schema(summary="Get one product by ID",
                   description="Retrieves **product**, returns 404 if not found.",
                   responses={
                       200: ProductSerializer,
                       404: OpenApiResponse(description="Empty response, product by ID not found."),
                   })
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)


# API
class OrderViewSet(ModelViewSet):
    """
    Набор представлений для действий над Order.

    Полный CRUD для действий над заказом.
    """

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [
        SearchFilter,
        DjangoFilterBackend,
        OrderingFilter,
    ]
    search_fields = ["delivery_address", "promocode"]
    filterset_fields = [
                        "products",
                        "delivery_address",
                        "promocode",
                        "user",
                        "created_at",
                        ]
    ordering_filters = [
        "user",
        "created_at",
        "archived",
    ]


class ShopIndexView(View):
    """ Главная страница. """

    def get(self, request: HttpRequest) -> HttpResponse:
        first_header = "Hello! This is a Shop Index page!"
        context = {
            "time_running": default_timer(),
            "first_header": first_header,
            "items": 1,
        }
        logger.info('Загружена главная страница')
        return render(request, "shopapp/shop-index.html", context=context)


class GroupsListView(View):
    """ Представление для списка групп пользователей и создания группы. """

    def get(self, request: HttpRequest) -> HttpResponse:
        context = {
            "form": GroupForm,
            "groups": Group.objects.prefetch_related('permissions').all(),
        }
        return render(request, "shopapp/groups-list.html", context=context)

    def post(self, request: HttpRequest):
        """ Создание группы. """
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(request.path)


class ProductDetailsView(DetailView):
    """ Представление для информации по товару. """

    template_name = "shopapp/product-details.html"
    # model = Product
    queryset = Product.objects.prefetch_related("images")
    # context = {
    #     "images": images,
    # }
    context_object_name = "product"
    # def get(self, request: HttpRequest, pk:int) -> HttpResponse:
    #     product = get_object_or_404(Product, pk=pk)
    #     context = {
    #         "product": product,
    #     }
    #     return render(request, "shopapp/product-details.html", context=context)


class ProductsListView(ListView):
    template_name = "shopapp/products-list.html"
    # model = Product
    context_object_name = "products"
    queryset = Product.objects.filter(archived=False)


class ProductCreateView(UserPassesTestMixin, CreateView):
    def test_func(self):
        # return self.user.groups.filter(name="secret_group").exists()
        return self.request.user.is_staff

    def form_valid(self, form):
        # success_url = self.get_success_url()
        form.instance.created_by = self.request.user
        logger.info("Загружена страница создания товара")
        return super().form_valid(form)

    model = Product
    fields = "name", "price", "description", "discount", "preview"
    success_url = reverse_lazy("shopapp:products_list")


class ProductUpdateView(UserPassesTestMixin, UpdateView):
    def test_func(self):
        object = self.get_object()
        req_perms = [
            "shopapp.change_product",
        ]
        return self.request.user.is_superuser or self.request.user == object.created_by and\
               self.request.user.has_perms(req_perms)

    model = Product
    fields = "name", "price", "description", "discount", "preview"
    template_name_suffix = "_update_form"

    def get_success_url(self):
        logger.info("Product updated successfully.")
        return reverse(
            "shopapp:product_details",
            kwargs={"pk": self.object.pk},
        )


class ProductDeleteView(DeleteView):
    model = Product
    success_url = reverse_lazy("shopapp:products_list")

    def form_valid(self, form):
        success_url = self.get_success_url()
        self.object.archived = True
        self.object.save()
        return HttpResponseRedirect(success_url)


class OrdersListView(LoginRequiredMixin, ListView):
    queryset = (
        Order.objects
        .select_related("user")
        .prefetch_related("products")
        .only("pk", "user", "user__username",
              "user__first_name", "delivery_address",
              "promocode", "created_at")
        .filter(archived=False)
    )
    paginate_by = 20
    ordering = ["pk"]


class OrderDetailView(PermissionRequiredMixin, DetailView):
    permission_required = "shopapp.view_order"
    queryset = (
        Order.objects.select_related("user").prefetch_related("products")
    )


class OrderCreateView(CreateView):
    model = Order
    fields = "user", "delivery_address", "promocode", "products"
    success_url = reverse_lazy("shopapp:orders_list")


class OrderUpdateView(UpdateView):
    model = Order
    fields = "user", "delivery_address", "promocode", "products"
    template_name_suffix = "_update_form"

    def get_success_url(self):
        logger.info("Order updated successfully.")
        return reverse(
            "shopapp:order_details",
            kwargs={"pk": self.object.pk},
        )


class OrderDeleteView(DeleteView):
    model = Order
    success_url = reverse_lazy("shopapp:orders_list")

    def form_valid(self, form):
        success_url = self.get_success_url()
        self.object.archived = True
        self.object.save()
        return HttpResponseRedirect(success_url)


class ProductsDataExportView(View):
    def get(self, request: HttpRequest) -> HttpResponse:
        products = Product.objects.order_by("pk").all()
        products_data = [
            {
                "pk": product.pk,
                "name": product.name,
                "price": product.price,
                "archived": product.archived,
            }
            for product in products
        ]
        logger.info("JSON with products was exported successfully.")
        return JsonResponse({"products": products_data})


class OrdersDataExportView(UserPassesTestMixin, View):
    def test_func(self):
        return self.request.user.is_staff

    def get(self, request: HttpRequest) -> HttpResponse:
        orders = Order.objects.filter(archived=False)
        orders_data = []
        for order in orders:
            product_ids = order.products.values_list("pk", flat=True)
            order_info = {
                "pk": order.pk,
                "delivery_address": order.delivery_address,
                "promocode": order.promocode,
                "user": order.user.pk,
                "products": list(product_ids),
            }
            orders_data.append(order_info)
        logger.info("Orders' data was exported successfully.")
        return JsonResponse({"orders": orders_data})
