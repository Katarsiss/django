from django.contrib import admin

from .models import Article, Tag, Author, Category


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = "pk", "title", "get_author", "content", "get_category"
    list_display_links = "pk", "title"

    def get_queryset(self, request):
        return (Article.objects
                .select_related('author', 'category')
                .prefetch_related('tags'))

    @property
    def content_short(self, obj: Article) -> str:
        if len(obj.content) < 48:
            return obj.content
        return obj.content[:48] + "..."

    @admin.display(description='Author')
    def get_author(self, obj):
        return obj.author.name

    @admin.display(description='Category')
    def get_category(self, obj):
        return obj.category.name

