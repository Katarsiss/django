import random
import unittest
from string import ascii_letters
from random import choices, choice

from django.conf import settings
from django.contrib.auth.models import User, Permission
from django.test import TestCase
from django.urls import reverse

from shopapp.models import Product, Order


class ProductCreateViewTestCase(TestCase):
    fixtures = [
        'dj-content-type-fixture.json',
        'auth_user_fixture.json',
        'products-fixture.json',
    ]

    def setUp(self) -> None:
        self.client.login(username="katar", password="1")
        self.product_name = "".join(choices(ascii_letters, k=10))
        Product.objects.filter(name=self.product_name).delete()

    def test_create_product(self):
        response = self.client.post(
            reverse("shopapp:create_product"),
            {
                "name": self.product_name,
                "price": "123.45",
                "description": "A good table",
                "discount": "10",
            }
        )
        self.assertRedirects(response, reverse("shopapp:products_list"))
        self.assertTrue(
            Product.objects.filter(name=self.product_name).exists()
        )


class ProductDetailsViewTestCase(TestCase):
    fixtures = [
        'dj-content-type-fixture.json',
        'auth_user_fixture.json',
        'products-fixture.json',
    ]
    @classmethod
    def setUpClass(cls):
        cls.product = Product.objects.create(name="Best Product")
    # def setUp(self) -> None:
    #     self.product = Product.objects.create(name="Best Product")

    @classmethod
    def tearDownClass(cls):
        Order.objects.filter(products=cls.product).delete()
        cls.product.delete()

    # def tearDown(self) -> None:
    #     self.product.delete()

    def test_get_product(self):
        response = self.client.get(reverse("shopapp:product_details", kwargs={"pk": self.product.pk}))
        self.assertEqual(response.status_code, 200)

    def test_get_product_and_check_content(self):
        response = self.client.get(reverse("shopapp:product_details", kwargs={"pk": self.product.pk}))
        self.assertContains(response, self.product.name)


class ProductListViewTestCase(TestCase):
    fixtures = [
        'dj-content-type-fixture.json',
        'auth_user_fixture.json',
        'products-fixture.json',
    ]

    def test_products(self):
        response = self.client.get(reverse("shopapp:products_list"))
        # Вариант 1
        # products = Product.objects.filter(archived=False).all()
        # products_ = response.context["products"]
        # for p, p_ in zip(products, products_):
        #     self.assertEqual(p.pk, p_.pk)
        # Вариант 2
        # self.assertQuerySetEqual(
        #     qs=Product.objects.filter(archived=False).all(),
        #     values=(p.pk for p in response.context["products"]),
        #     transform=lambda p: p.pk,
        # )
        # Вариант 3
        for product in Product.objects.filter(archived=False).all():
            self.assertContains(response, product.name)
        self.assertTemplateUsed(response, 'shopapp/products-list.html')


class OrdersListViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.credentials = dict(username="BobTest", password="qwerty")
        cls.user = User.objects.create_user(**cls.credentials)

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.login(**self.credentials)
        # self.client.force_login(self.user)

    def test_orders_view(self):
        response = self.client.get(reverse("shopapp:orders_list"))
        self.assertContains(response, "Orders")

    def test_orders_view_not_authenticated(self):
        self.client.logout()
        response = self.client.get(reverse("shopapp:orders_list"))
        # self.assertRedirects(response, str(settings.LOGIN_URL)) - Редирект с /?next=/
        self.assertEqual(response.status_code, 302)
        self.assertIn(str(settings.LOGIN_URL), response.url)


class ProductsExportViewTestCase(TestCase):
    fixtures = [
        'dj-content-type-fixture.json',
        'auth_user_fixture.json',
        'products-fixture.json',
    ]

    def test_get_products_view(self):
        response = self.client.get(reverse("shopapp:products_export"))
        self.assertEqual(response.status_code, 200)
        products = Product.objects.order_by("pk").all()
        for product in products:
            print(f"- {product.name}")
        expected_data = [
            {
                "pk": product.pk,
                "name": product.name,
                "price": str(product.price),
                "archived": product.archived,
            }
            for product in products
        ]
        products_data = response.json()
        self.assertEqual(
            products_data["products"],
            expected_data,
        )


class OrderDetailViewTestCase(TestCase):
    fixtures = [
        'dj-content-type-fixture.json',
        'auth_user_fixture.json',
        'products-fixture.json',
        'orders-fixture.json'
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        user_data = {'username': "KateTest", 'password': "qwerty"}
        cls.user = User.objects.create_user(**user_data)
        permission = Permission.objects.get(codename="view_order")
        cls.user.user_permissions.add(permission)

        print("User Details:")
        print(f"Username: {cls.user.username}")
        user_permissions = cls.user.user_permissions.all()
        print("User Permissions:")
        for permission in user_permissions:
            print(f"- {permission}")

    def setUp(self) -> None:
        self.client.force_login(self.user)
        products = Product.objects.filter(archived=False).all()
        print("Products:")
        for product in products:
            print(f"- {product.name}")
        product = products.first()
        # self.order_product = random.choice(products)
        if product:
            print(f"Selected Product: {product.name}")
            self.order = Order.objects.create(
                user=self.user,
                delivery_address="A new delivery address",
                promocode="SUM23"
            )
            self.order.products.set([product])
            print("Order created successfully")
        else:
            print("No products found")

        print('ТЕСТ')

        # self.order = Order.objects.create(
        #     user=self.user,
        #     delivery_address="A new delivery address",
        #     promocode="SUM23",
        # )

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.user.delete()

    def tearDown(self) -> None:
        self.order.delete()

    def test_order_details(self):
        response = self.client.get(reverse('shopapp:order_details', kwargs={'pk': self.order.pk}))
        self.assertContains(response, self.order.delivery_address)
        self.assertContains(response, self.order.promocode)
        self.assertEqual(response.context["order"].pk, self.order.pk)


class OrdersExportTestCase(TestCase):
    fixtures = [
        'dj-content-type-fixture.json',
        'auth_user_fixture.json',
        'products-fixture.json',
        'orders-fixture.json'
    ]

    @classmethod
    def setUpClass(cls):
        cls.credentials = dict(username="KateTest", password="qwerty")
        cls.user = User.objects.create_user(**cls.credentials)
        cls.user.is_staff = True
        cls.user.save()

        print("User Details:")
        print(f"Username: {cls.user.username}")
        print(f"Is Staff: {cls.user.is_staff}")

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.login(**self.credentials)

    def test_get_order_list(self):
        response = self.client.get(reverse("shopapp:orders_export"))
        # Просто для отладки
        if response.wsgi_request.user.is_authenticated:
            print("User is authenticated.")
        else:
            print("User is not authenticated.")
        self.assertEqual(response.status_code, 200)
        orders = Order.objects.filter(archived=False)
        expected_data = []
        for order in orders:
            product_ids = order.products.values_list("pk", flat=True)
            order_info = {
                "pk": order.pk,
                "delivery_address": order.delivery_address,
                "promocode": order.promocode,
                "user": order.user.pk,
                "products": list(product_ids),
            }
            expected_data.append(order_info)
            orders_data = response.json()
            self.assertEqual(
                orders_data["orders"],
                expected_data
            )
