import profile

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, ListView
from django.utils.translation import gettext_lazy as _, ngettext_lazy

from myauth.models import Profile
from .forms import ProfileForm
from .models import Profile
from django.views import View


class HelloView(View):
    welcome_message = _("Welcome Hello World!")

    def get(self, request: HttpRequest) -> HttpResponse:
        items_str = request.GET.get("items") or 0
        items = int(items_str)
        products_line = ngettext_lazy(
            "one product",
            "{count} products",
            items,
        )
        products_line = products_line.format(count=items)
        return HttpResponse(f"<h1>{self.welcome_message}</h1>"
                            f"\n<h2>{products_line}</h2>")


class AboutMeView(TemplateView):
    template_name = "myauth/about-me.html"


class RegisterView(CreateView):
    form_class = UserCreationForm
    template_name = "myauth/register.html"
    success_url = reverse_lazy("myauth:about-me")

    def form_valid(self, form):
        response = super().form_valid(form)
        Profile.objects.create(user=self.object)
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")

        user = authenticate(self.request,
                            username=username,
                            password=password,
                            )
        login(request=self.request, user=user)
        return response

    def get_success_url(self):
        user = self.object
        success_url = reverse_lazy("myauth:about-me")
        return success_url


def login_view(request: HttpRequest) -> HttpResponse:
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect("/admin/")

        return render(request, 'myauth/login.html')
    username = request.POST["username"]
    password = request.POST["password"]

    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect("/admin/")
    return render(request, "myauth/login.html", {"error": "Invalid login credentials"})


class MyLogoutView(LogoutView):
    next_page = reverse_lazy("myauth:login")

# def logout_view(request: HttpRequest) -> HttpResponse:
#     logout(request)
#     return redirect(reverse("myauth:login"))


class ProfileUpdateView(UserPassesTestMixin, UpdateView):
    model = Profile
    template_name = "myauth/profile_update_form.html"
    form_class = ProfileForm

    def get_object(self, queryset=None):
        profile_user = get_object_or_404(User, pk=self.kwargs['pk'])
        profile, created = Profile.objects.get_or_create(user=profile_user)
        return profile

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Получение предыдущей страницы из HTTP_REFERER
        previous_page = self.request.META.get('HTTP_REFERER')
        context['previous_page'] = previous_page
        return context

    def get_success_url(self):
        previous_page = self.request.POST.get('previous_page')
        if previous_page:
            return previous_page
        return reverse_lazy("myauth:profile", kwargs={"pk": profile.user.pk})

    def test_func(self):
        return self.request.user.is_staff or self.get_object().user == self.request.user


class UserProfileListView(ListView):
    model = User
    template_name = "myauth/user_list.html"
    context_object_name = "user_list"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        users = User.objects.all()
        context['users'] = users
        return context


class ProfileView(LoginRequiredMixin, TemplateView):
    model = Profile
    template_name = "myauth/profile.html"
    context_object_name = "profile"

    def get(self, request, *args, **kwargs):
        profile_user = get_object_or_404(User, pk=kwargs['pk'])
        profile = profile_user.profile
        context = {
            'profile': profile
        }
        return render(request, self.template_name, context)


@user_passes_test(lambda u: u.is_superuser)
def set_cookie_view(request: HttpRequest) -> HttpResponse:
    response = HttpResponse("Cookie set")
    response.set_cookie("fizz", "buzz", max_age=3600)
    return response


def get_cookie_view(request: HttpRequest) -> HttpResponse:
    value = request.COOKIES.get("fizz", "default value")
    return HttpResponse(f"Cookie value: {value!r}")


@permission_required("myauth.view_profile", raise_exception=True)
def set_session_view(request: HttpRequest) -> HttpResponse:
    request.session["foobar"] = "spameggs"
    return HttpResponse("Session set!")


@login_required
def get_session_view(request: HttpRequest) -> HttpResponse:
    value = request.session.get("foobar", "default")
    return HttpResponse(f"Session value: {value!r}")


class FooBarView(View):
    def get(self, request: HttpRequest) -> JsonResponse:
        return JsonResponse({"foo": "bar", "spam": "eggs"})
