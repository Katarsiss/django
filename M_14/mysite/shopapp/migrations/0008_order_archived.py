# Generated by Django 4.2.1 on 2023-05-17 21:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopapp', '0007_alter_product_options_alter_order_delivery_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='archived',
            field=models.BooleanField(default=False),
        ),
    ]
