from django.urls import path, include
from .views import (
    ArticlesListView,
)

app_name = "BlogApp"

urlpatterns = [
    path("articles/", ArticlesListView.as_view(), name="articles"),
]
