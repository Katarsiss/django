from django.core import serializers
from django.http import HttpResponse, HttpResponseBadRequest, HttpRequest
from django.shortcuts import render
from django.views.generic import ListView, DetailView

from BlogApp.models import Article
import logging


logger = logging.getLogger(__name__)


class ArticlesListView(ListView):
    template_name = "BlogApp/articles_list.html"
    context_object_name = "articles"
    queryset = (Article.objects
                .select_related('author', 'category')
                .prefetch_related('tags')
                .defer('content')
                .order_by("pk"))


def get_articles_in_custom_format(request: HttpRequest) -> HttpResponse:
    format = request.GET['format']
    if format not in ['xml', 'json', 'yaml']:
        return HttpResponseBadRequest()
    data = serializers.serialize(format, Article.objects.all())
    return HttpResponse(data)


class ArticlesDetailView(DetailView):
    model = Article
    template_name = 'BlogApp/article_detail.html'
