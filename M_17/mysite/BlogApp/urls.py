from django.urls import path, include
from .views import (
    ArticlesListView,
    get_articles_in_custom_format,
    ArticlesDetailView,
)

from .feeds import LatestArticlesFeed

app_name = "BlogApp"

urlpatterns = [
    path("", get_articles_in_custom_format, name="articles"),
    path("articles/", ArticlesListView.as_view(), name="articles"),
    path("articles/<int:pk>/", ArticlesDetailView.as_view(), name="article"),
    path('latest/feed/', LatestArticlesFeed(), name="rss"),
]
