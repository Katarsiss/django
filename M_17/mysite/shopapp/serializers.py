from rest_framework import serializers
from .models import Product, Order


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("pk",
                  "name",
                  "description",
                  "price",
                  "discount",
                  "created_at",
                  "archived",
                  "is_instock",
                  "preview",
                  )


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ("pk",
                  "products",
                  "delivery_address",
                  "promocode",
                  "user",
                  "created_at",
                  "archived",
                  )
