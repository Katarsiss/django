from django.contrib.sitemaps import Sitemap

from shopapp.models import Product, Order


class ProductSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        return Product.objects.filter(archived=False)

    def lastmod(self, obj: Product):
        return obj.created_at


class OrderSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        return (Order.objects
                .select_related("user")
                .prefetch_related("products")
                .only("pk", "user", "user__username",
                      "user__first_name", "delivery_address",
                      "promocode", "created_at")
                .filter(archived=False))

    def lastmod(self, obj: Order):
        return obj.created_at

