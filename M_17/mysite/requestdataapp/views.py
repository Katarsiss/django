from django.core.files.storage import FileSystemStorage
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

from .forms import UserBioForm


def process_get_view(request: HttpRequest) -> HttpResponse:
    first_line = request.GET.get("first_line", "")
    second_line = request.GET.get("second_line", "")
    result = first_line + second_line
    context = {
        "first_line": first_line,
        "second_line": second_line,
        "result": result,
    }
    return render(request, "requestdataapp/request-query-params.html", context=context)


def user_form(request: HttpRequest) -> HttpResponse:
    context = {
        "form": UserBioForm(),
    }
    return render(request, "requestdataapp/user-bio-form.html", context=context)


def handle_file_upload(request: HttpRequest) -> HttpResponse:
    error = None
    if request.method == "POST" and request.FILES.get("myfile"):
        myfile = request.FILES["myfile"]
        if myfile.size > 1048576:
            error = "You can't upload a file over 1 Mb."
        else:
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            print("saved file", filename)
    context = {"error": error}
    return render(request, "requestdataapp/file-upload.html", context=context)
